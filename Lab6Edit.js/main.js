
function changeColor(){
  let elementHtml = document.querySelector("#squareDiv");
  elementHtml.classList.add("square-red");
  elementHtml.classList.remove("square-green");

}

function removeColor(){
  let elementHtml = document.querySelector("#squareDiv");
  elementHtml.classList.remove("square-red");
  

}
function addNewColor(){
  let elementHtml = document.querySelector("#squareDiv");
  elementHtml.classList.remove("square-red");
  elementHtml.classList.add("square-green");
}

let square = document.querySelector("#squareDiv");
square.addEventListener("mouseenter",changeColor);
square.addEventListener("mouseleave",addNewColor);