//Recolher n valores do utilizador por intermedio do prompt
//Processar cada valor e dizer para o ecrã se ele é par ou impar

let listaNumeros = [];
let continuar = "s";

while(continuar === "s"){
  let numeroRecolhido = prompt("Digite o seu numero");
  listaNumeros.push(numeroRecolhido);
  continuar = prompt("Quer inserir mais numeros? (s/n)");


}

for(i=0;i<listaNumeros.length;++i){
  let numeroEmAnalise = listaNumeros[i];
  let resto = numeroEmAnalise%2;
  if( resto === 0){
    alert("o numero introduzido " + numeroEmAnalise + " é par");
  }
  else{
    alert("o numero introduzido " + numeroEmAnalise + " é impar");
  }
}

/*
//o for seria a mesma coisa que esta resolução com while
let i=0;

while(i < listaNumeros.length){
   let numeroEmAnalise = listaNumeros[i];
  let resto = numeroEmAnalise%2;
  if( resto === 0){
    alert("o numero introduzido " + numeroEmAnalise + " é par");
  }
  else{
    alert("o numero introduzido " + numeroEmAnalise + " é impar");
  }
  ++i;

}
*/




