let listValue =  []; // declarar array vazio
let listValue2 = new Array(); //declarar array vazio;

let vogais = ["a","e","i","o","u"]; //declarar um array com 5 valores
let vogais2 = new Array("a","e","i","o","u");//declarar um array com 5 valores

alert(vogais[1]); //mostra o que se encontra na posição 2 do array 

vogais[1] = "I"; //altera o valor da posição 2

alert(vogais[1]);

//mostrar no ecrã atraves de alert cada posição do array
/*
  alert(vogais2[0]);
  alert(vogais2[1]);
  alert(vogais2[2]);
  alert(vogais2[3]);
  alert(vogais2[4]);
*/

for(index=0; index < vogais2.length ; ++index){
  alert(vogais2[index]);
}

